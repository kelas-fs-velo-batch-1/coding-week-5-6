<?php

use App\Http\Controllers\Controller;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\CourseContentController;
use App\Http\Controllers\ExamController;
use App\Http\Controllers\AnswerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', [Controller::class, 'routes'])->name('route information');
Route::get('/example', [Controller::class, 'example'])->name('example route');
Route::get('/error', [Controller::class, 'error'])->name('error route');

Route::get('/post', [Controller::class, 'post'])->name('post route');

Route::post('/auth/register', [RegisterController::class, 'register']);
Route::post('/auth/login', [LoginController::class, 'login']);
Route::get('/test', function() {
    return "hello";
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/auth/logout', [LoginController::class, 'logout']);
    Route::get('/auth/user', [Controller::class, 'user']);
    Route::resource('user', UserController::class);
    Route::resource('course', CourseController::class);
    Route::resource('course-content', CourseContentController::class);
    Route::resource('exam', ExamController::class);
    Route::resource('answer', AnswerController::class);
});
