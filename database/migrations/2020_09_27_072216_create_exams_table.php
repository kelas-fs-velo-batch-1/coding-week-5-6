<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('exam_code');
            $table->string('exam_name');
            $table->text('description')->nullable();
            $table->foreignId('user_id');
            $table->foreignId('course_id')->nullable()->index();
            $table->dateTime('available_from');
            $table->dateTime('available_to');
            $table->jsonb('configurations')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->index(['available_from','available_to']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}
