// state
export const state = () => ({
  exams: [],
})

// getters
export const getters = {
  exams: state => state.exams,
}

// mutations
export const mutations = {
  LOAD_EXAMS (state, exams) {
    state.exams[exams.id] = exams
  },
}

// actions
export const actions = {
  async fetchExam (context, id) {
    if (context.getters['exams'][id]) {
      console.log("Loaded exam from cache")
      return context.getters['exams'][id]
    }
    let exam  = (await this.$axios.$get("exam/" + id, )).data
    context.commit('LOAD_EXAMS', exam)
    return exam
  },
}