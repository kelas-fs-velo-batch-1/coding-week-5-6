import Vue from 'vue'
// state
export const state = () => ({
  answer: {},
  answer_ids: {},
})

// getters
export const getters = {
  answer: state => state.answer,
  answer_ids: state => state.answer_ids,
}

// mutations
export const mutations = {
  SET_ANSWER (state, {exam_id, question_id, answer}) {
    if (!state.answer[exam_id]) {
      Vue.set(state.answer, exam_id, {})
    }
    Vue.set(state.answer[exam_id], question_id, answer)
  },
  SET_ANSWER_ID (state, {exam_id, answer_id}) {
    Vue.set(state.answer_ids, exam_id, answer_id)
  },
  DELETE_ANSWER_ID (state, {exam_id}) {
    Vue.delete(state.answer_ids, exam_id)
    Vue.delete(state.answer, exam_id)
  },
}

// actions
export const actions = {
  async setAnswer (context, {exam_id, question_id, answer}) {
    console.log('SET_ANSWER', {exam_id, question_id, answer})
    context.commit('SET_ANSWER', {exam_id, question_id, answer})
    await this.$axios.$patch("answer/" + context.getters['answer_ids'][exam_id], {
      answers: context.getters['answer'][exam_id]
    })
  },
  async submitAnswer (context, {exam_id}) {
    let resp = await this.$axios.$patch("answer/" + context.getters['answer_ids'][exam_id], {
      is_submit: true
    })
    context.commit('DELETE_ANSWER_ID', {exam_id})
    return resp
  },
  async startExam (context, payload) {
    let answer = (await this.$axios.$post("answer", payload)).data
    context.commit('SET_ANSWER_ID', {exam_id: payload.exam_id, answer_id: answer.code})
  },
}