export default ({ $axios, $toast, app, store, redirect }) => {
  if (process.server) {
    return
  }

  $axios.onRequest(config => {
    const token = store.getters["auth/token"]
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`
    }
  })

  // Response interceptor
  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if(code >= 500) {
      $toast.show({
        type: 'danger',
        title: 'Terjadi kesalahan',
        message: 'Error dari server',
      })
      console.log('error', error)
    }
    if (code === 401 && store.getters["auth/check"]) {
      $toast.show({
        type: 'danger',
        title: 'Logout',
        message: 'Sesi Anda telah berakhir',
      })
      store.commit("auth/LOGOUT")
      redirect("/auth/login")
    }
  })
}
