const permissions = {
  'users-params': 'user.list',
  'users-view-id': 'user.list',
  'users-create': 'user.create',
  'users-edit-id': 'user.update',
}

export default async ({ store, redirect, route }) => {
  const user = store.getters['auth/user']
  if (permissions[route.name]) {
    if (!store.getters['auth/check']) {
      window.previousPath = route.fullPath
      return redirect('/auth/login')
    }
    if (user.all_permissions.indexOf(permissions[route.name]) == -1) {
      return redirect('/auth/unauthorized')
    }
  }
}
