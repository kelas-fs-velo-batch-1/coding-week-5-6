<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use HasFactory, SoftDeletes;
    protected $casts = [
        'configurations' => 'array',
    ];

    public function questions() {
        return $this->hasMany(Question::class);
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
