<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;

    protected $casts = [
        'answers' => 'array',
        'is_submit' => 'boolean',
        'code' => 'string',
    ];
    protected $guarded = [
        'created_at',
        'updated_at',
    ];
}
