<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use acidjazz\metapi\MetApi;

use App\Models\Exam;
use App\Models\User;
use App\Models\Answer;
use App\Models\Question;
use DB;

class AnswerController extends Controller
{
    use MetApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Exam::get();

        return $this->render($models);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        DB::beginTransaction();
        if (!$user) {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'profile' => $request->profile,
                'password' => bcrypt($request->password),
                'is_guest' => true,
            ]);
        }
        $answer = Answer::create([
            'code' => random_int(0, PHP_INT_MAX),
            'user_id' => $user->id,
            'exam_id' => $request->exam_id,
            'answers' => [],
            'is_submit' => false,
        ]);
        DB::commit();

        return $this->render($answer);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $answer = Answer::with(['exam'])->first();

        return $this->render($answer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::where('code', $id)->first();
        if (!$answer) {
            return $this->render([
                'success' => false,
                'message' => 'Answer not found'
            ]);
        }
        if ($request->has('is_submit')) {
            $answer->is_submit = true;
        }
        if ($request->has('answers')) {
            $answer->answers = $request->answers;
        }
        $answer->save();
        return $this->render([
            'success' => true
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function error()
    {
        return $this->render(['forced_error' => $forced_error]);
    }
}
