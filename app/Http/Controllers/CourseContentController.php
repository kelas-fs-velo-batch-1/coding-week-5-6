<?php

namespace App\Http\Controllers;

use App\Models\CourseContent;
use App\Models\Course;
use Illuminate\Http\Request;
use acidjazz\metapi\MetApi;

class CourseContentController extends Controller
{
    use MetApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.list')) {
            abort(403);
        }
        $models = CourseContent::query();

        $searchparams = json_decode($request->searchparams, true);
        $page = 1;
        
        if ($searchparams && is_array($searchparams)) {
            foreach ($searchparams as $key => $value) {
                switch ($key) {
                    case 'search':
                        if (!empty($value)) {
                            $models->where(function($q) use ($value) {
                                $q->where('title', 'like', "%$value%");
                            });
                        }
                        break;
                    case 'course_id':
                        $models->where('course_id', $value);
                        $course = Course::find($value);
                        break;
                    case 'page':
                        $page = $value;
                        break;
                }
            }
        }

        $count = $models->count();
        $perpage = $request->get('perpage',10);
        $last_page = ceil($count / $perpage);

        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as &$model) {
            $model->created_at_str = date('j F Y H:i', strtotime($model->created_at));
        }

        $this->addMeta('course', $course);
        $this->addMeta('pagination', [
            'total' => $count,
            'per_page' => $perpage,
            'current_page' => $page,
            'last_page' => $last_page,
            'first_item' => ($page-1) * $perpage + 1,
            'last_item' => min(($page) * $perpage, $count),
            'pages' => getPaginationArray($page, $last_page),
        ]);

        return $this->render($models);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.create')) {
            abort(403);
        }
        $courseContent = CourseContent::create([
            'type' => $request->type,
            'title' => $request->title,
            'course_id' => $request->course_id,
            'content' => $request->content,
        ]);
        return $this->render($courseContent);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CourseContent  $courseContent
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, CourseContent $courseContent)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.list')) {
            abort(403);
        }
        return $this->render($courseContent);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CourseContent  $courseContent
     * @return \Illuminate\Http\Response
     */
    public function edit(CourseContent $courseContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CourseContent  $courseContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CourseContent $courseContent)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.edit')) {
            abort(403);
        }
        $courseContent->title = $request->title;
        $courseContent->type = $request->type;
        $courseContent->content = $request->content;
        $courseContent->save();
        return $this->render($courseContent);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CourseContent  $courseContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, CourseContent $courseContent)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.delete')) {
            abort(403);
        }
        $courseContent->delete();
    }
    public function error()
    {
        return $this->render(['forced_error' => $forced_error]);
    }
}
