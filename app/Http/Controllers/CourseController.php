<?php

namespace App\Http\Controllers;

use App\Models\Course;
use Illuminate\Http\Request;
use acidjazz\metapi\MetApi;

class CourseController extends Controller
{
    use MetApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.list')) {
            abort(403);
        }
        $models = Course::query();

        $searchparams = json_decode($request->searchparams, true);
        $page = 1;
        
        if ($searchparams && is_array($searchparams)) {
            foreach ($searchparams as $key => $value) {
                switch ($key) {
                    case 'search':
                        if (!empty($value)) {
                            $models->where(function($q) use ($value) {
                                $q->where('course_level', 'like', "%$value%")
                                    ->orWhere('name', 'like', "%$value%");
                            });
                        }
                        break;
                    case 'page':
                        $page = $value;
                        break;
                }
            }
        }

        $count = $models->count();
        $perpage = $request->get('perpage',10);
        $last_page = ceil($count / $perpage);

        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as &$model) {
            $model->created_at_str = date('j F Y H:i', strtotime($model->created_at));
        }

        $this->addMeta('pagination', [
            'total' => $count,
            'per_page' => $perpage,
            'current_page' => $page,
            'last_page' => $last_page,
            'first_item' => ($page-1) * $perpage + 1,
            'last_item' => min(($page) * $perpage, $count),
            'pages' => getPaginationArray($page, $last_page),
        ]);

        return $this->render($models);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.create')) {
            abort(403);
        }
        $course = Course::create([
            'course_level' => $request->course_level,
            'name' => $request->name,
        ]);
        return $this->render($course);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Course $course)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.list')) {
            abort(403);
        }
        return $this->render($course);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit(Course $course)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Course $course)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.edit')) {
            abort(403);
        }
        $course->course_level = $request->course_level;
        $course->name = $request->name;
        $course->save();
        return $this->render($course);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Course $course)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('course.delete')) {
            abort(403);
        }
        $course->delete();
    }
    public function error()
    {
        return $this->render(['forced_error' => $forced_error]);
    }
}
