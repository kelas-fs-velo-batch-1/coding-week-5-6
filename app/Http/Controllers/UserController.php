<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use acidjazz\metapi\MetApi;

class UserController extends Controller
{
    use MetApi;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('user.list')) {
            abort(403);
        }
        $models = User::query();

        $searchparams = json_decode($request->searchparams, true);
        $page = 1;
        
        if ($searchparams && is_array($searchparams)) {
            foreach ($searchparams as $key => $value) {
                switch ($key) {
                    case 'search':
                        if (!empty($value)) {
                            $models->where(function($q) use ($value) {
                                $q->where('username', 'like', "%$value%")
                                    ->orWhere('email', 'like', "%$value%")
                                    ->orWhere('name', 'like', "%$value%");
                            });
                        }
                        break;
                    case 'email':
                        if (!empty($value)) {
                            $models->where($key, $value);
                        }
                        break;
                    case 'page':
                        $page = $value;
                        break;
                }
            }
        }

        $count = $models->count();
        $perpage = $request->get('perpage',10);
        $last_page = ceil($count / $perpage);

        $models = $models->skip(($page-1) * $perpage)->take($perpage)->get();

        foreach ($models as &$model) {
            $model->created_at_str = date('j F Y H:i', strtotime($model->created_at));
        }

        $this->addMeta('pagination', [
            'total' => $count,
            'per_page' => $perpage,
            'current_page' => $page,
            'last_page' => $last_page,
            'first_item' => ($page-1) * $perpage + 1,
            'last_item' => min(($page) * $perpage, $count),
            'pages' => getPaginationArray($page, $last_page),
        ]);

        return $this->render($models);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user();
        if (!$user->hasPermissionTo('user.create')) {
            abort(403);
        }
        $user = User::create([
            'username' => $request->username,
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'is_guest' => false,
        ]);
        return $this->render($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        $u = $request->user();
        if (!$u->hasPermissionTo('user.list')) {
            abort(403);
        }
        return $this->render($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $u = $request->user();
        if (!$u->hasPermissionTo('user.edit')) {
            abort(403);
        }
        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        if (!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }
        $user->save();
        return $this->render($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, User $user)
    {
        $u = $request->user();
        if (!$u->hasPermissionTo('user.delete')) {
            abort(403);
        }
        $user->delete();
    }
    public function error()
    {
        return $this->render(['forced_error' => $forced_error]);
    }
}
